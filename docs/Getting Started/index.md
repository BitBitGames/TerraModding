To mod Terraform you'll need at least basic knowledge of Javascript.

If you're just starting out, try Codecadamy or Grasshopper for the very basics.

# Creating the base files.

In a new empty folder, create a file called `main.js`, this is the main script. This is a required script, and your mod will not work if this file does not exist. Once you've created the file, you can use the editor of your choice. I recommend VS Code, or Notepad++. You'll start by creating three meta-data variables. These are the only things required to get started.

```js
var title = "example-mod";
var version = "0.0.1";
var description = "My first mod!";
```

And you're all set!

# Compiling your mod.

Once you've created your mod, simply zip up all the files/folders. Then you'll change the extension of the zip to `.terra`, so `example-mod.zip` would be called `example-mod.terra`.

Drop the terra file into /.terraform/mods and run Terraform.
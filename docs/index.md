We're working on making modding Terraform as easy as possible, and we hope that our docs reflect that.

Give [Getting Started](/gettingstarted) a read, and within minutes you know how to make a shell for your mod.